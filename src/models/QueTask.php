<?php
namespace gradus\que\models;

use Yii;
use Exception;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\db\Query;
//use app\helpers\LogHelper;

class QueTask extends ActiveRecord
{
	const STATUS_NORMAL = 0;
	const STATUS_PROCESSING = 1;
	const STATUS_PROCESSED = 2;
	const STATUS_FAILED = 3;    
    
    public static $reconnectionRepeats = 3;

    private static $_defaultRepeats = 0;

    private static $table_for_processed = '{{%que_task_processed}}';

    public static function tableName()
    {
        return '{{%que_task}}';
    }

    public function rules()
    {
        return [
            ['command', 'string', 'max' => 255],
            ['command_substr', 'string', 'max' => 255],
            ['command_hash', 'integer'],
            ['execute_time', 'double'],
            ['task_id', 'integer'],
            ['command_output', 'safe'],
            ['server', 'safe'],
            ['return_var', 'safe'],
            ['retries', 'integer'],
            ['status', 'integer'],
            ['priority', 'integer'],
        ];
    }

	public function beforeSave($insert)
	{
		if (!parent::beforeSave($insert)) {
			return false;
		}
	
		if($this->isNewRecord) {
			$this->command_substr = ($pos = strpos($this->command, ' '))?substr($this->command, 0, $pos):$this->command;
			$this->command_hash = QueTask::StrToNum($this->command_substr);
		}
		// ...custom code here...
		return true;
	}

    public static function StrToNum($str, $range = false) {
        $str = substr(md5($str), 0, 16);
        $num = '';
        for ($i = 0; $i < strlen($str); $i++) {
            $num .= ord($str[$i]) % 10;
        }

        if($range)
            return (int)($num) % $range;
        else {
            return (int)($num);
        }
    }

    public static function add($attributes)
    {
        $model = new self;
        $model->setAttributes($attributes);
        
        $model->save();
        return $model;
    }
    
    public static function getQuedTypes() {
		$query = (new Query())
            ->select([
               'command_hash' => new Expression('DISTINCT command_hash'),
            ])
            ->from(QueTask::tableName())
			;

		$data = $query->column();
		$types = [];
		
		foreach($data as $v) {
			$type = QueTask::find()
				->select('command_substr')
				->where(['command_hash' => $v])
				->limit(1)
				->scalar()
				;
				
			if($type) $types[] = $type;
		}
		
		return $types;
    }

    public static function getProcessingCount($command_substr = null) {
		$query = (new Query())
            ->from(QueTask::tableName())
            ->andWhere(['status'=>QueTask::STATUS_PROCESSING])
            ;

		if($command_substr) {
            $query->andWhere(['command_substr'=>$command_substr]);
		}

    	return $query->count();
    }

    public static function resetNotProcessed()
    {
    	$max_started = 1;
    	//@todo make it job-specific, specify in job table max_processing time.
    	$max_processing = 5;
		$count = 0;
		
		//restart tasks that failed to start
		$count  += \Yii::$app->db->createCommand()
			->update(QueTask::tableName(), [
				'retries' => new Expression('retries + 1'),
				'rand'=>null,
				'started_at' => null,
				'command_output' => new \yii\db\Expression(' CONCAT(\'\n!!!<<<RESTARTED, FAILED TO START>>>\n\', command_output) '),
				], 'status = '.QueTask::STATUS_NORMAL.' AND rand is not null AND started_at < (NOW() - INTERVAL '.$max_started.' MINUTE)')
			->execute();
		
		//restart tasks that failed to finish in $max_processing minutes
		$count  += \Yii::$app->db->createCommand()
			->update(QueTask::tableName(), [
				'retries' => new Expression('retries + 1'),
				'rand'=>null,
				'started_at' => null,
				'status' => QueTask::STATUS_NORMAL,
				'command_output' => new \yii\db\Expression(' CONCAT(\'\n!!!<<<RESTARTED, FAILED TO FINISH TIMEOUT>>>\n\', command_output) '),
				], 'status = '.QueTask::STATUS_PROCESSING.' AND started_at < (NOW() - INTERVAL '.$max_processing.' MINUTE)')
			->execute();

		return $count;		
    }

    public static function deleteOldProcessed()
    {
		//delete older then 1 week
		\Yii::$app->db->createCommand(
			'DELETE FROM '.QueTask::tableName().' WHERE created_at < NOW() - INTERVAL 2 HOUR;'
			)
			->execute();
    }

    public static function moveProcessed()
    {	
    	//@todo remove
//     	set_time_limit(60*60*1);
// 		ini_set('memory_limit','10G');
        
        $start = microtime(true);

        $setSize = 1000;

        $total = 0;
        $inserted = 0;
        $deleted = 0;

        while ( $portion = self::find()->where(['status' => QueTask::STATUS_PROCESSED])->limit($setSize)->asArray()->all() ) {
            
			$total += count($portion);
			$toDel = array_column($portion, 'id');

			try {
				$sql = "INSERT INTO ".QueTask::$table_for_processed." (SELECT * FROM ".QueTask::tableName()." WHERE id IN (".implode(', ', $toDel)."));";
				$inserted += \Yii::$app->db->createCommand($sql)->execute();
			} catch (\Exception $e) {
		        //LogHelper::log("moveProcessed:".$e->getMessage(), "@runtime/logs/", 'job_moving_processed.log');
			}

			try {
				$sql = "DELETE FROM ".QueTask::tableName()." WHERE id IN (".implode(', ', $toDel).");";
				$deleted += \Yii::$app->db->createCommand($sql)->execute();
			} catch (\Exception $e) {
		        //LogHelper::log("moveProcessed:".$e->getMessage(), "@runtime/logs/", 'job_moving_processed.log');
			}

        }

        //LogHelper::log("Total $total, Inserted $inserted, Deleted $deleted (".round(microtime(true) - $start, 2)." sec)", "@runtime/logs/", 'job_moving_processed.log');
        
        return $deleted;
    }

    public static function getNotProcessed($job_type = null)
    {
        $rand = rand(-2147483648,2147483648);

		$query = (new Query())
            ->select('id')
            ->from(QueTask::tableName())
            ->andWhere(['status'=>QueTask::STATUS_NORMAL])
            ->andWhere('rand IS NULL')
            ->orderBy('priority ASC, created_at ASC')
            ->limit(1)
            ;

        if($job_type) {
        	$query
				->andWhere(['command_substr'=>$job_type])
				;
        }
        
        $job_id = $query->scalar();

        if( $job_id >0 ) {

            \Yii::$app->db->createCommand()
                ->update(QueTask::tableName(), ['rand'=>$rand, 'started_at' => new Expression('NOW()')], 'id = '.$job_id.' AND rand IS NULL')
                ->execute();

            if($job = self::find()->where(['id'=>$job_id, 'rand'=>$rand, 'status' => QueTask::STATUS_NORMAL])->limit(1)->one()) {
                $job->status = QueTask::STATUS_PROCESSING;
                $job->rand = null;
                $job->server = gethostname();
                $job->save();
                    
                return $job;
            }

        }

    }

    public function release()
    {
		$this->status = QueTask::STATUS_NORMAL;
		$this->rand = null;
		$this->save();
    }
    
	public static function addedInPeriod($cmd, $period, $timeUnit = "SECOND")
    {
        $condition = 'command = :cmd AND removed_from_queue_at > DATE_SUB(NOW(), INTERVAL :period ' . $timeUnit . ')';
        self::$_defaultRepeats = self::$reconnectionRepeats > self::$_defaultRepeats ? self::$reconnectionRepeats : self::$_defaultRepeats;

        try {

            $count = self::find()
                ->where($condition, [':cmd' => $cmd, ':period' => $period])
                ->count();

            self::$reconnectionRepeats = self::$_defaultRepeats;
            return $count;

        } catch (Exception $e) {

            if (self::$reconnectionRepeats > 0) {

                self::$reconnectionRepeats--;
                Yii::$app->db->close();
                Yii::$app->db->open();

                return self::addedInPeriod($cmd, $period, $timeUnit);

            } else {

                throw $e;

            }
        }
    }

    public static function getJobsDetails($status = QueTask::STATUS_NORMAL, $min = 0) 
    {
        $res['total'] = \Yii::$app->db->createCommand('SELECT COUNT(*) FROM '.QueTask::tableName().' WHERE status='.$status.' '.($min>0?'AND started_at > DATE_SUB(NOW(), INTERVAL '.$min.' MINUTE)':''))
             ->queryScalar();

		$query = (new Query())
            ->select([
                'total_count' => new Expression('COUNT(*)'),
                'command_hash',
// 				'command_substr' => new Expression('ANY_VALUE(command_substr)'),
            ])
            ->from(QueTask::tableName())
			->where(['status' => $status])
			->groupBy('command_hash')
			;
			
		if($min>0) {
			$query->andWhere('started_at > DATE_SUB(NOW(), INTERVAL :min MINUTE)',  [':min' => $min]);
		}
		$data = $query->all();
		
		foreach($data as $k=>$v) {
			$data[$k]['command_substr'] =
				QueTask::find()
				->select('command_substr')
				->where(['command_hash' => $v['command_hash']])
				->limit(1)
				->scalar()
				;
		}
		
		$res['types'] = $data;

        return $res;
    }

    public static function getJobsPerServerCount($min = 0) 
    {
        return \Yii::$app->db->createCommand('SELECT COUNT(*) AS total_count, server
            FROM '.QueTask::tableName().'
            WHERE status='.QueTask::STATUS_PROCESSED.' '.($min>0?'AND started_at > DATE_SUB(NOW(), INTERVAL '.$min.' MINUTE)':'').'
            GROUP BY server
            ORDER BY total_count DESC')
            ->queryAll();
    }

    public static function getJobs($status = 0, $command_substr = '', $min = 0, $retries = 0) 
    {
        $commands = \Yii::$app->db->createCommand('SELECT id, command, command_output, execute_time, server, retries, return_var, created_at, started_at 
            FROM '.QueTask::tableName().' 
            WHERE '.($status!==''?'status = '.$status:'1').' '.($retries?'AND retries>0':'').' '.($command_substr?'AND command_substr = \''.$command_substr.'\'':'').' '.($min>0?'AND started_at > DATE_SUB(NOW(), INTERVAL '.$min.' MINUTE)':'').' ORDER BY started_at DESC LIMIT 25')
            ->queryAll();
        return $commands;
    }

    public static function getFailedJobsCount($hours = 24)
    {
        return \Yii::$app->db->createCommand('SELECT COUNT(*) FROM '.QueTask::tableName().' WHERE status='.QueTask::STATUS_FAILED.' AND started_at > DATE_SUB(NOW(), INTERVAL '.$hours.' HOUR)')
             ->queryScalar();
    }

    public static function getJobsWithRetriesCount($hours = 24) 
    {
        return \Yii::$app->db->createCommand('SELECT COUNT(*) FROM '.QueTask::tableName().' WHERE retries>0 AND started_at > DATE_SUB(NOW(), INTERVAL '.$hours.' HOUR)')
             ->queryScalar();
    }
}
