<?php
namespace gradus\que;
use Yii;
use yii\base\BootstrapInterface;
class Bootstrap implements BootstrapInterface{
    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules([
            'monitor' => 'que/monitor/index',
        ], false);
        $app->setModule('que', 'gradus\que\Module');
    }
}