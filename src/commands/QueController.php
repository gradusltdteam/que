<?php

//namespace app\commands;
namespace gradus\que\commands;

use app\helpers\ConsoleHelper;
use PDO;
use Yii;
use yii\console\Controller;
use yii\helpers\Json;
//use app\helpers\ProfilingHelper;
//use app\helpers\LogHelper;
//use app\models\JobLog;
use gradus\que\models\QueTask;
use app\components\Daemon;

class QueController extends Controller
{

	//const DB_LOG_NAME = 'que';

	public $host,
		   $load_limit,
		   $process_limit,
		   $job_types,
		   $profile,
		   $timer_1,
		   $timer_2,
		   $timer_3,
		   $timer_4;

	public function actionReset()
	{

		if($count = QueTask::resetNotProcessed()) {
			$message = $count . ' tasks restarted!'.PHP_EOL;
			echo $message;
			//LogHelper::dbLog($message, self::DB_LOG_NAME);
		}

		//echo 'test';	
		echo PHP_EOL;
	}
        
    public function actionRunWorker()
    {
		$this->host = gethostname();
		$this->load_limit = 15;
		$this->process_limit = 100;

		$this->job_types = [];

		$this->profile = true;
		//or else long running script slows down "exec"
// 		\Yii::$app->db->enableLogging = false;
// 		\Yii::$app->db->enableProfiling = false;


        while(true) {
        	$this->worker();
        }
    }
    
    public function worker(){
		$start_jobs = 0;

		try {
			//$this->profile = ProfilingHelper::inline('Start', $this->profile);
			//conditions safe to run task

			//cpu
			$load = sys_getloadavg()[0];
			if($load > $this->load_limit) {
				throw new \Exception('CPU load is '.$load);
			}
			//$this->profile = ProfilingHelper::inline('check load', $this->profile);

			if(1){//if($this->host == 'go18.ufobe.com') {
				$timer_now = microtime(true);

				//1min
				if(!isset($this->timer_1) || $timer_now - $this->timer_1 >= 60) {
					$this->timer_1 = $timer_now;
					if($count = QueTask::resetNotProcessed()) {
						$message = $count . ' tasks restarted!'.PHP_EOL;
						echo $message;
						//LogHelper::dbLog($message, self::DB_LOG_NAME);
					}
					//$this->profile = ProfilingHelper::inline('db: reset not processed', $this->profile);
				}

				//1hour
				if(!isset($this->timer_2) || $timer_now - $this->timer_2 >= 60*60*1) {
					$this->timer_2 = $timer_now;
					//@todo is it ok?
					QueTask::moveProcessed();
					//$this->profile = ProfilingHelper::inline('db: move processed', $this->profile);
				}

				//1min or if empty
				if(!isset($this->timer_3) || $timer_now - $this->timer_3 >= 60 || !count($this->job_types)) {
					$this->timer_3 = $timer_now;
					
					$this->job_types = array_unique(array_merge($this->job_types, QueTask::getQuedTypes()));
					//$this->profile = ProfilingHelper::inline('db: get que types', $this->profile);
				}

				//10min
				if(!isset($this->timer_4) || $timer_now - $this->timer_4 >= 60 * 15) {
					$this->timer_4 = $timer_now;
					gc_collect_cycles();
					//$this->profile = ProfilingHelper::inline('!!Garbadge collector', $this->profile);
				}
			}

			//proc count??
			//$count = ConsoleHelper::process_run_count('que/run-task '.\Yii::$app->id);
			$count = QueTask::getProcessingCount();
			//echo $count.PHP_EOL;

			if($count >= $this->process_limit) {
				throw new \Exception('Number of tasks '.$count);
			}
			//$this->profile = ProfilingHelper::inline('check proc count', $this->profile);
			//have mysql job

			//echo count($this->job_types).PHP_EOL;
			foreach($this->job_types as $job_type)
			//@todo make limit per task type. check = 50. scrape = 5. CF = 20, parse = 50
			if($job = QueTask::getNotProcessed($job_type)) {
				//$this->profile = ProfilingHelper::inline('get task', $this->profile);
				//print_r($job);

				try {
					$this->checkAndSkip($job, 'semrush/scrape', 10);
					$this->checkAndSkip($job, 'semrush/cf503', 50);
					
					//$this->profile = ProfilingHelper::inline('check for skip', $this->profile);
					//$this->checkAndSkip($job, 'semrush/parse', 50);
				} catch(\Exception $e) {
					echo $e->getMessage()."\n";
					continue;
				}

				//execute task
				$start_jobs++;
// 		        	echo '+';
				$cmd = 'php yii que/que/run-task '.escapeshellarg(\Yii::$app->id).' '.$job->id.' > /dev/null 2>&1 &';
				//echo $cmd.PHP_EOL;
// 				$cmd = 'nohup php yii que/run-task '.escapeshellarg(\Yii::$app->id).' '.$job->id.'</dev/null > /dev/null 2>&1 &';
				$result = null;
				exec($cmd, $result);
				//$this->profile = ProfilingHelper::inline('exec task', $this->profile);
			}
			//$this->profile = ProfilingHelper::inline('finish', $this->profile);
			
		} catch(\Exception $e) {
			echo $e->getMessage()."\n";
		}
		//10ms
		//small sleep to spread jobs a bit
// 			usleep(10 * 1000);
// 			$this->profile = ProfilingHelper::inline('short sleep', $this->profile);

		if(!$start_jobs) {
			//nothing found, no rush for next
			sleep(5);
			//$this->profile = ProfilingHelper::inline('long sleep, no job can be started', $this->profile);
		}
    }
    
    public function checkAndSkip($job, $command_substr, $limit) {
		if($job->command_substr == $command_substr) {
			//$count = ConsoleHelper::process_run_count($archive_command);
			$count = QueTask::getProcessingCount($command_substr);

			if($count >= $limit) {
				$job->release();
				throw new \Exception('Skip ' . $command_substr . '. Too much running: ' . $count);
			}
		}
    }

    public function actionRunTask($app_id, $id)
    {
    	//find task
    	if($job = QueTask::find()->where(['id' => $id])->one()) {
			$start_time = microtime(true);
			
			$command = "php yii ".$job->command;
			//echo $command.PHP_EOL;

			exec($command, $output, $return_var);
			$job->return_var = $return_var;
			//echo $return_var.PHP_EOL;
			$job->command_output = gethostname().' '.Yii::$app->formatter->asDatetime(time(), 'php:Y-m-d H:i:s').' <<<'.PHP_EOL.implode(PHP_EOL, $output).PHP_EOL.'>>>'.PHP_EOL.$job->command_output;
			//echo $job->command_output.PHP_EOL;
			$job->status = QueTask::STATUS_PROCESSED;
			//echo $job->status.PHP_EOL;
			$job->execute_time = microtime(true) - $start_time;
			//echo $job->execute_time.PHP_EOL;
			$job->server = gethostname();
			//echo $job->server.PHP_EOL;
			// if return_var bad -> and reties < 5 -> status=0;
			if($job->return_var !== 0) {
				if($job->retries <= 5 && $job->return_var != 2) {
					$job->status = QueTask::STATUS_NORMAL;
					$job->retries++;
				} else {
					$job->status = QueTask::STATUS_FAILED;
				}
			}
			//echo $job->status.PHP_EOL;
			$job->save();
			
			//$text = date('Y-m-d H:i:s') .' | ';
			/*$text = '';
			$text .= $job->server .' | ';
			$text .= 'job id #'.$job->id;
			$text .= ' | ';
			$text .= sprintf("Elapsed: %f", $job->execute_time);    	
			$text .= ' | ';
			$text .= 'return_var: '.$job->return_var;
			$text .= ' | ';
			$text .= '';
			$text .= PHP_EOL;
			$text .= '$'.$command;
			$text .= PHP_EOL;*/
			//@todo revert - change type to LONGTEXT
			//$text .= $job->command_output;
			//LogHelper::dbLog($text, self::DB_LOG_NAME);
    	} else {
			//LogHelper::dbLog($id.' task not found', self::DB_LOG_NAME);
    	}
    }
}