<?php

namespace gradus\que\helpers;

use gradus\que\models\QueTask;

class QueHelper
{
    const defaultPriority = 100;

    public static function addTask($command, $priority = null)
    {

        $priority = isset($priority) ? $priority : QueHelper::defaultPriority;

		$job = QueTask::add([
			'command' => $command,
			'priority' => $priority,
		]);

		$res = $job->id;
		
        return $res;
    }
}
