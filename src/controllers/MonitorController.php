<?php

namespace gradus\que\controllers;

use Yii;
use yii\web\Controller;
use gradus\que\models\QueTask;

class MonitorController extends Controller
{
    public function actionIndex($status = 0, $command = '', $min = 0, $retries = 0)
    {
        $commands = QueTask::getJobs($status, $command, $min, $retries);
        return $this->render('index', compact('commands', 'status', 'command', 'retries'));
    }
}