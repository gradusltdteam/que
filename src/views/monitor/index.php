<?php

use gradus\que\models\QueTask;
use yii\helpers\Url;
use yii\helpers\Html;

$action = \Yii::$app->request->get('action');
if($action == 'cleanup') {
	QueTask::moveProcessed();
}

$this->title = "Que monitor";

$min = 30;
$hour = 24;

$normalJobs = QueTask::getJobsDetails(QueTask::STATUS_NORMAL);
$processingJobs = QueTask::getJobsDetails(QueTask::STATUS_PROCESSING);
$lastJobs = QueTask::getJobsDetails(QueTask::STATUS_PROCESSED, $min);
$perServer = QueTask::getJobsPerServerCount($min);

?>
<style>
.low {
    height: 1.5em;
    overflow: hidden;
    cursor: pointer;
}
</style>
<script type="text/javascript">
function stretch(el) {
    $(el).css("height","auto");
    return false;
}
</script>
<div class="panel panel-default">
    <div class="panel-body">
        <h3>Que monitor <?php echo Html::a('cleanup', ['', 'action'=>'cleanup'])?></h3>
        <div class="row">
            <div class="col-md-6">
                <?php echo ($status==(string)QueTask::STATUS_NORMAL&&$command==''?'<b>':'<a href="'.Url::to(['', 'status' => QueTask::STATUS_NORMAL]).'">'); ?>Total normal in que:&nbsp;<?php echo $normalJobs['total']; ?><?php echo ($status==(string)QueTask::STATUS_NORMAL&&$command==''?'</b>':'</a>'); ?><br>
                <?php foreach($normalJobs['types'] as $id=>$type):?>
                    <?php echo ($status==QueTask::STATUS_NORMAL&&$command==$type['command_substr']?'<b>':'<a href="'.Url::to(['', 'status' => QueTask::STATUS_NORMAL, 'command' => $type['command_substr']]).'">'); ?><?php echo $type['command_substr'] ?>:&nbsp;<?php echo $type['total_count']; ?><?php echo ($status==QueTask::STATUS_NORMAL&&$command==$type['command_substr']?'</b>':'</a>'); ?><br/>
                <?php endforeach?>
                <br/>
                <?php echo ($status==QueTask::STATUS_PROCESSING&&$command==''?'<b>':'<a href="'.Url::to(['', 'status' => QueTask::STATUS_PROCESSING]).'">'); ?>Total processing in que:&nbsp;<?php echo $processingJobs['total']; ?><?php echo ($status==QueTask::STATUS_PROCESSING&&$command==''?'</b>':'</a>'); ?><br>
                <?php foreach($processingJobs['types'] as $id=>$type):?>
                    <?php echo ($status==QueTask::STATUS_PROCESSING&&$command==$type['command_substr']?'<b>':'<a href="'.Url::to(['', 'status' => QueTask::STATUS_PROCESSING, 'command' => $type['command_substr']]).'">'); ?><?php echo $type['command_substr'] ?>:&nbsp;<?php echo $type['total_count']; ?><?php echo ($status==QueTask::STATUS_PROCESSING&&$command==$type['command_substr']?'</b>':'</a>'); ?><br/>
                <?php endforeach?>
                <br/>
                <?php echo ($status==QueTask::STATUS_PROCESSED&&$command==''?'<b>':'<a href="'.Url::to(['', 'status' => QueTask::STATUS_PROCESSED, 'min' => $min]).'">'); ?>Last processed jobs (<?php echo $min;?>min):&nbsp;<?php echo $lastJobs['total']; ?><?php echo ($status==QueTask::STATUS_PROCESSED&&$command==''?'</b>':'</a>'); ?><br>
                <?php foreach($lastJobs['types'] as $id=>$type):?>
                    <?php echo ($status==QueTask::STATUS_PROCESSED&&$command==$type['command_substr']?'<b>':'<a href="'.Url::to(['', 'status' => QueTask::STATUS_PROCESSED, 'command' => $type['command_substr'], 'min' => $min]).'">'); ?><?php echo $type['command_substr'] ?>:&nbsp;<?php echo $type['total_count']; ?><?php echo ($status==QueTask::STATUS_PROCESSED&&$command==$type['command_substr']?'</b>':'</a>'); ?><br/>
                <?php endforeach?>
            </div>
            <div class="col-md-6">
                <?php echo ($status==QueTask::STATUS_FAILED&&$command==''?'<b>':'<a href="'.Url::to(['', 'status' => QueTask::STATUS_FAILED, 'min' => $hour*60]).'">'); ?>Failed for last <?=$hour?>h: &nbsp;<?php echo QueTask::getFailedJobsCount(); ?><?php echo ($status==QueTask::STATUS_FAILED&&$command==''?'</b>':'</a>'); ?>
                <br><br>
                <?php echo ($status==''&&$retries==1?'<b>':'<a href="'.Url::to(['', 'status' => '', 'min' => $hour*60, 'retries' => 1]).'">'); ?>Several retries for last <?=$hour?>h (all statuses):&nbsp<?php echo QueTask::getJobsWithRetriesCount(); ?><?php echo ($status==''&&$retries==1?'</b>':'</a>'); ?>
                <br><br>
                <label>Jobs per server (<?php echo $min;?>min)</label>
                <br>
                <?php foreach($perServer as $server):?>
                    <b><?php echo $server['server']; ?>:</b>&nbsp;<?php echo $server['total_count']; ?><br/>
                <?php endforeach?>
            </div>
        </div>
        <br/>
        <?php foreach($commands as $job):?>
			<?php echo $job['id'].' '.$job['server'].': '.$job['command'].'<br>Created at '.Yii::$app->formatter->asDatetime($job['created_at'], 'php:Y-m-d H:i:s').' ('.Yii::$app->formatter->asRelativeTime($job['created_at']).')<br>Started at '.Yii::$app->formatter->asDatetime($job['started_at'], 'php:Y-m-d H:i:s').' ('.Yii::$app->formatter->asRelativeTime($job['started_at']).')<br>'.($job['command_output']?'<div class="low" onclick="return stretch(this);">> '.nl2br($job['command_output']).'</div>':'').($job['return_var']!=0?'<span style="color:red;">ERROR: </span>':'').'Return var: '.$job['return_var'].'<br>('.round($job['execute_time'], 2).' sec, '.$job['retries'].' retries)<br><br>'; ?>
        <?php endforeach?>
    </div>
</div>