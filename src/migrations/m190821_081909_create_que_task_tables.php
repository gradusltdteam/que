<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m190821_081909_create_que_task_tables
 */
class m190821_081909_create_que_task_tables extends Migration
{

    public $table = '{{%que_task}}';
    public $table_processed = '{{%que_task_processed}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $fields = [
            'id' => Schema::TYPE_BIGPK,
            'command' => $this->string(255)->defaultValue(null),
            'command_substr' => $this->string(255)->defaultValue(null),
            'command_hash' => $this->bigInteger(20)->defaultValue(null),
            'priority' => $this->integer()->defaultValue(0),
            'status' => $this->tinyInteger(1)->defaultValue(0),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'started_at' => $this->timestamp()->defaultValue(null),
            'task_id' => $this->integer()->defaultValue(null),
            'execute_time' => $this->float()->defaultValue(null),
            'command_output' => $this->getDb()->getSchema()->createColumnSchemaBuilder('longtext'),
            'return_var' => $this->string(255)->notNull()->defaultValue(''),
            'retries' => $this->integer(1)->defaultValue(0),
            'server' => $this->string(255)->defaultValue(null),
            'rand' => $this->integer()->defaultValue(null),
        ];

        $this->createTable($this->table, $fields, $tableOptions);
        $this->createIndexes($this->table);

        $this->createTable($this->table_processed, $fields, $tableOptions);
        $this->createIndexes($this->table_processed);

    }

    public function createIndexes($tableName)
    {
        $this->createIndex('rand_2', $tableName, ['rand','status','started_at']);
        $this->createIndex('status', $tableName, ['status','started_at']);
        $this->createIndex('status_2', $tableName, ['status','rand','created_at','priority']);
        $this->createIndex('status_rand_command_substr_priority_created_at', $tableName, ['status','rand','command_substr','priority','created_at']);
        $this->createIndex('status_rand_command_substr', $tableName, ['status','rand','command_substr']);
        $this->createIndex('command_hash', $tableName, ['command_hash']);
        $this->createIndex('status_command_hash', $tableName, ['status','command_hash']);
        $this->createIndex('status_started_at_command_hash', $tableName, ['status','started_at','command_hash']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
        $this->dropTable($this->table_processed);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190821_081909_create_que_task_tables cannot be reverted.\n";

        return false;
    }
    */
}
