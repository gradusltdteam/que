<?php
namespace gradus\que;
use yii\base\Module as BaseModule;
class Module extends BaseModule
{
    public $controllerNamespace = 'gradus\que\controllers';
    public function init()
	{
	    parent::init();
	    if (\Yii::$app instanceof \yii\console\Application) {
	        $this->controllerNamespace = 'gradus\que\commands';
	    }
	}
}