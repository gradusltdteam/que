To connect locally
---
composer.json:
```
"repositories": [
       {
           "type": "path",
           "url": "/path/to/yii2-que"
       }
]
```

And then:
```
composer require gradus/yii2-que:dev-master --prefer-source
```

Apply Migration
---
```
yii migrate
```


Using
---
que monitor:
```
yoursite.com/monitor OR yoursite.com/index.php?r=que/monitor/index
```

to add new task:
```
use gradus\que\helpers\QueHelper;
QueHelper::addTask($command)
```

to run worker:
```
yii que/que/run-worker
```